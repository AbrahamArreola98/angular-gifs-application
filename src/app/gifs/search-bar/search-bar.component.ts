import { Component, ElementRef, ViewChild } from '@angular/core';
import { GifsService } from '../services/gifs.service';

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
})
export class SearchBarComponent {
    constructor(private gifsService: GifsService) {}

    @ViewChild('searchTxt') searchTxt!: ElementRef<HTMLInputElement>;

    search() {
        const value = this.searchTxt.nativeElement.value;

        this.gifsService.searchGifs(value);

        console.log(this.gifsService.history);

        this.searchTxt.nativeElement.value = '';
    }
}
