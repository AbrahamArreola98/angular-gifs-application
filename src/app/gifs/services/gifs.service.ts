import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchGIFResponse } from '../interfaces/gifs.interface';

@Injectable({
    providedIn: 'root',
})
export class GifsService {
    private apiKey = 'kO6NgT0WDPol9DkbevPKGmXdxQzOgTuY';
    private serviceUrl = 'https://api.giphy.com/v1/gifs/search';
    private _history: string[] = [];

    results: Gif[] = [];

    constructor(private httpClient: HttpClient) {
        this._history = JSON.parse(localStorage.getItem('history')!) || [];
        this.results = JSON.parse(localStorage.getItem('results')!) || [];
    }

    get history(): string[] {
        return [...this._history];
    }

    searchGifs(query: string = '') {
        if (!query) {
            return;
        }

        const normalizedQuery = query.trim().toLocaleLowerCase();
        if (!this._history.includes(normalizedQuery)) {
            this._history.unshift(normalizedQuery);
            this._history = this.history.splice(0, 10);

            localStorage.setItem('history', JSON.stringify(this._history));
        }

        const params = new HttpParams()
            .set('api_key', this.apiKey)
            .set('limit', '10')
            .set('q', query);

        this.httpClient
            .get<SearchGIFResponse>(`${this.serviceUrl}`, { params })
            .subscribe((resp) => {
                this.results = resp.data;
                localStorage.setItem('results', JSON.stringify(this.results));
            });
    }
}
